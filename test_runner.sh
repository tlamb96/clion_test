#!/bin/sh

# Execute tests.
./simple_math_test

mkdir ./test_coverage 2>/dev/null

# Generate test coverage report html.
gcovr -r . --html --html-details -o ./test_coverage/test_coverage_report.html

