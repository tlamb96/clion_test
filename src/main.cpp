#include <iostream>
#include <memory>
#include "SimpleMath.h"


#ifndef TESTING
int main() {
    std::cout << "Hello, World!" << std::endl;
    auto simpleMath = std::make_unique<SimpleMath>();
    int add_val = simpleMath->add(1, 2);
    std::cout << add_val << std::endl;
    return 0;
}
#endif