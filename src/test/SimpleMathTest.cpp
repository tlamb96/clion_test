//
// Created by tyler on 2/26/19.
//

#include "gtest/gtest.h"
#include "../SimpleMath.cpp"

TEST(AddTest, Add)
{
    SimpleMath sm{};
    EXPECT_EQ(3, sm.add(1,2));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}