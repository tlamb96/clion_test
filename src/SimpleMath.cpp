//
// Created by tyler on 2/19/19.
//

#include <iostream>
#include "SimpleMath.h"

SimpleMath::SimpleMath() {
    std::cout << "In SimpleMath constructor." << std::endl;
}

int SimpleMath::add(int num_1, int num_2) {
    return num_1 + num_2;
}
