FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    gcc \
    g++ \
    cmake \
    git-all \
    wget \
    gcovr

# Create non-root user.
RUN useradd -m clion && \
  adduser clion sudo && \
  echo "clion:password" | chpasswd

# Setup user environment.
USER clion
WORKDIR /home/clion
ENV HOME /home/clion

# Install recent version of CMake.
RUN cd /home/clion && \
    wget --quiet https://github.com/Kitware/CMake/releases/download/v3.13.4/cmake-3.13.4-Linux-x86_64.tar.gz && \
    tar -xf cmake-3.13.4-Linux-x86_64.tar.gz
ENV PATH "/home/clion/cmake-3.13.4-Linux-x86_64/bin:${PATH}"

# Setup Google Test.
RUN cd /home/clion && \
    git clone https://github.com/google/googletest.git && \
    cd googletest/googletest/ && \
    cmake CMakeLists.txt && \
    make
ENV GTEST_PATH "/home/clion/googletest/googletest"

# Clone the repository then build.
RUN git clone https://gitlab.com/tlamb96/clion_test.git && \
    cd ./clion_test && \
    cmake CMakeLists.txt && \
    make

# Run tests and generate test coverage results.
RUN ./test_runner.sh

CMD ["/bin/bash"]
